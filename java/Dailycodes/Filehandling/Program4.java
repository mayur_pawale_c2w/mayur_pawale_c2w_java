import java.io.*;
class Employee implements Serializable{
	int EmpId;
	String empName;

	Employee(int EmpId,String empName){
		this.EmpId=EmpId;
		this.empName=empName;

	}
}

class serialDemo{
	public static void main(String[]args)throws IOException{
		FileOutputStream fos=new FileOutputStream("Emp.txt");
		ObjectOutputStream oos=new ObjectOutputStream(fos);

		Employee obj1=new Employee(1,"Chutan");
		Employee obj2=new Employee(2,"Mayur");

		oos.writeObject(obj1);
		oos.writeObject(obj2);

		oos.close();
		fos.close();
	}
}
class DeserializeDemo{
	public static void main(String[]args)throws Exception{
		FileInputStream fis=new FileInputStream("Emp.txt");
		ObjectInputStream ois=new ObjectInputStream(fis);
		Employee Fileobj1=(Employee)ois.readObject();
		Employee Fileobj2=(Employee)ois.readObject();

		System.out.println( Fileobj1.EmpId);
		System.out.println( Fileobj1.empName);
		System.out.println( Fileobj2.EmpId);
		System.out.println( Fileobj2.empName);



	}
}




			

		

