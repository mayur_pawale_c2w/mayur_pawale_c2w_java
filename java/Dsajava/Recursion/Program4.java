class RecursionDemo{

	void numPrint(int num){

		if(num==0){

			return;
		}
		
		numPrint(num--);

		System.out.println(num);

	}
	public static void main(String[]args){

		RecursionDemo obj=new RecursionDemo();
		obj.numPrint(10);

	}
}
