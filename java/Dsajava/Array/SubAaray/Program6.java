class ArrayDemo{
        public static void main(String[]args){
                int arr[]=new int[]{2,4,1,3};
                int n=4;
		int prefixsum[]=new int[arr.length];
		prefixsum[0]=arr[0];

		for(int i=1;i<n;i++){
			prefixsum[i]=prefixsum[i-1]+arr[i];
		}


                for(int i=0;i<arr.length;i++){

                        for(int j=i;j<arr.length;j++){
                                int sum=0;
				if(i==0){
					sum=prefixsum[j];
				}
				else{
					sum=prefixsum[j]-prefixsum[i-1];
				}

      					
                               System.out.println(sum);
                        }
                }
        }
}
//Time complexity=O(N^2);
//Space Comlexity=O(N);
//
//
//
//
///////////////////////////////////////////////////////////
class ArrayDemo1{
        public static void main(String[]args){
                int arr[]=new int[]{2,4,1,3};
                int prefixsum[]=new int[arr.length];
                prefixsum[0]=arr[0];

               
                for(int i=0;i<arr.length;i++){
			int sum=0;
                        for(int j=i;j<arr.length;j++){
                               
				sum+=arr[j];
                                
                               System.out.println(sum);
                        }
                }
        }
}
}
//Time complexity=O(N^2);
//Space Comlexity=O(1);
