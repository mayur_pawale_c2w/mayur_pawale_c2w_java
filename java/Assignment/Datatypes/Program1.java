//1. A student is in class 11th and there are 12 divisions of the class which data type is feasible to print the class and divisions. (Write a code to print the class and division).


class ClassDivisionPrinter {
    public static void main(String[] args) {
        String className = "11th";
        int numDivisions = 12;

        for (int division = 1; division <= numDivisions; division++) {
            String divisionName = "Division " + division;
            System.out.println("Class: " + className + ", " + divisionName);
        }
    }
}
